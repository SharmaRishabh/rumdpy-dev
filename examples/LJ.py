import numpy as np
import rumdpy as rp
from numba import cuda
import pandas as pd
import pickle
import json
import sys
#import matplotlib.pyplot as plt

include_rdf = True
if 'NoRDF' in sys.argv:
    include_rdf = False
    
integrator = 'NVE'
if 'NVT' in sys.argv:
    integrator = 'NVT'
if 'NVT_Langevin' in sys.argv:
    integrator = 'NVT_Langevin'


# Generate configuration with a FCC lattice
c1 = rp.make_configuration_fcc(nx=8,  ny=8,  nz=8,  rho=0.8442, T=1.44)      # N =  2*1024
#c1 = rp.make_configuration_fcc(nx=16,  ny=16,  nz=16,  rho=0.8442, T=1.44)  # N = 16*1024
c1.copy_to_device() 

pdict = {'N':c1.N, 'D':c1.D, 'simbox':c1.simbox.data, 'integrator':integrator, 'rdf':include_rdf}
print(pdict)
with open('Data/LJ_pdict.pkl', 'wb') as f:
    pickle.dump(pdict, f)

compute_plan = rp.get_default_compute_plan(c1)
print('compute_plan: ', compute_plan)

# Make pair potential
pair_potential = rp.apply_shifted_force_cutoff(rp.make_LJ_m_n(12,6))
params = [[[4.0, -4.0, 2.5],], ]
LJ = rp.PairPotential(c1, pair_potential, params=params, max_num_nbs=1000, compute_plan=compute_plan)
pairs = LJ.get_interactions(c1, exclusions=None, compute_plan=compute_plan, verbose=True)

# Make integrator
dt = 0.005
T0 = rp.make_function_constant(value=0.7) # Not used for NVE

if integrator=='NVE':
    integrate, integrator_params = rp.setup_integrator_nve(c1, pairs['interactions'], dt=dt, compute_plan=compute_plan, verbose=False)
        
if integrator=='NVT':
    integrate, integrator_params = rp.setup_integrator_nvt(c1, pairs['interactions'], T0, tau=0.2, dt=dt, compute_plan=compute_plan, verbose=False) 
        
if integrator=='NVT_Langevin':
    integrate, integrator_params = rp.setup_integrator_nvt_langevin(c1, pairs['interactions'], T0, alpha=0.1, dt=dt, seed=2023, compute_plan=compute_plan, verbose=False)

# Make rdf calculator
if include_rdf:
    num_bins = 500
    full_range = True
    rdf_bins = np.zeros(num_bins, dtype=np.float64)
    d_rdf_bins = cuda.to_device(rdf_bins)
    host_array_zeros = np.zeros(d_rdf_bins.shape, dtype=d_rdf_bins.dtype)
    rdf_calculator = rp.make_rdf_calculator(c1, pair_potential = LJ, compute_plan=compute_plan, 
                                            full_range=full_range, verbose=True)  

#  Run Simulation 
scalars_t = []
tt = []

inner_steps = 1000
steps = 500

start = cuda.event()
end = cuda.event()
zero = np.float32(0.0)

for i in range(steps+1):
    if i==1:
        start.record() # Exclude first step from timing to get it more precise by excluding time for JIT compiling
        
    integrate(c1.d_vectors, c1.d_scalars, c1.d_ptype, c1.d_r_im, c1.simbox.d_data,  pairs['interaction_params'], integrator_params, zero, inner_steps)
    
    if i>0:
        scalars_t.append(np.sum(c1.d_scalars.copy_to_host(), axis=0))
        tt.append(i*inner_steps*dt)

    if i>0 and include_rdf:
        rdf_calculator(c1.d_vectors, c1.simbox.d_data, c1.d_ptype, pairs['interaction_params'], d_rdf_bins)
        temp_host_array = d_rdf_bins.copy_to_host()       # offloading data from device and resetting decive array to zero. 
        rdf_bins += temp_host_array                       # ... (prevents overflow errors for longer runs)  
        d_rdf_bins = cuda.to_device(host_array_zeros)


end.record()
end.synchronize()
timing_numba = cuda.event_elapsed_time(start, end)
nbflag = LJ.nblist.d_nbflag.copy_to_host()    
tps = steps*inner_steps/timing_numba*1000

print('\tsteps :', steps*inner_steps)
print('\tnbflag : ', nbflag)
print('\ttime :', timing_numba/1000, 's')
print('\tTPS : ', tps )

# Save data
df = pd.DataFrame(np.array(scalars_t), columns=c1.sid.keys())
df['t'] = np.array(tt)
df.to_csv('Data/LJ_scalars.csv')

if include_rdf:
    data = rp.normalize_and_save_rdf(rdf_bins, c1, pairs['interaction_params'], 
                                    full_range, steps, filename='Data/LJ_rdf.dat')

# Plot results
import analyze_LJ



